1. Apakah perbedaan antara JSON dan XML?
- XML menyimpan data sebagai tree structure, sedangkan JSON menyimpan data dalam format map/dictionary di mana terdapat key dan value.
- XML adalah bahasa mark up, bukan bahasa pemrograman, sedangkan JSON hanyalah format yang ditulis dalam JavaScript.
- XML dapat melakukan pemrosesan dan pemformatan dokumen dan objek, sedangkan JSON tidak melakukan pemrosesan atau perhitungan apa pun.
- XML filenya besar dan lambat dalam penguraian yang menyebabkan transmisi data lebih lambat, sedangkan JSON sangat cepat karena ukuran file sangat kecil dan dilakukan oleh mesin JavaScript.
2. Apakah perbedaan antara HTML dan XML?
- XML tidak boleh ada error, sedangkan HTML error kecil dapat diabaikan.
- XML harus menggunakan tag penutup, sedangkan HTML bisa diberikan atau tidak.
- XML diperuntukkan untuk transfer informasi, sedangkan HTML diperuntukkan untuk penyajian data.
- XML harus melakukan nesting atau urutan dengan benar, sedangkan HTML tidak terlalu memperhitungkan. 

Sumber:
- https://www.monitorteknologi.com/perbedaan-json-dan-xml/
- https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html
